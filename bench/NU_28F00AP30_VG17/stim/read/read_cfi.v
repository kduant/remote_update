//          _/             _/_/
//        _/_/           _/_/_/
//      _/_/_/_/         _/_/_/
//      _/_/_/_/_/       _/_/_/              ____________________________________________ 
//      _/_/_/_/_/       _/_/_/             /                                           / 
//      _/_/_/_/_/       _/_/_/            /                                 28F00AP30 / 
//      _/_/_/_/_/       _/_/_/           /                                           /  
//      _/_/_/_/_/_/     _/_/_/          /                                     1Gbit / 
//      _/_/_/_/_/_/     _/_/_/         /                                single die / 
//      _/_/_/ _/_/_/    _/_/_/        /                                           / 
//      _/_/_/  _/_/_/   _/_/_/       /                  Verilog Behavioral Model / 
//      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.7 / 
//      _/_/_/    _/_/_/ _/_/_/     /                                           /
//      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2010 Numonyx B.V. / 
//      _/_/_/      _/_/_/_/_/    /___________________________________________/ 
//      _/_/_/       _/_/_/_/      
//      _/_/          _/_/_/  
// 
//     
//             NUMONYX              
// Include General define
`include "include/def.h"
// Include Characterization of the device
`include "include/data.h"
// Include CUI command define
`include "stim/AllTasks.h"



module  stimulus(A, DQ, W_N, G_N, E_N, L_N, K, RP_N, WP_N, VDD, VDDQ, VPP, Information);
   
   // Signal Bus
  output [`ADDRBUS_dim - 1:0] A;         // Address Bus 
  output [`DATABUS_dim - 1:0] DQ;        // Data I/0 Bus
  // Control Signal
  output W_N;                            // Write Enable 
  output G_N;                            // Output Enable
  output E_N;                       // Chip Enable
  output RP_N;                           // Reset/Power-Down
  output WP_N;                           // Write protect

  // Voltage signal rappresentad by integer Vector which correspond to millivolts
  output [`Voltage_range] VDD;                      // Supply Voltage
  output [`Voltage_range] VDDQ;                     // Input/Output Supply Voltage
  output [`Voltage_range] VPP;                      // Optional supply voltage
  
  // Others Signal
  
  output K;                               // clock
  output L_N;                             // latch enable
  
  
 output Information;                    // Enable/Disable Information of the operation in the memory 
   
  reg [`ADDRBUS_dim - 1:0] AddrBus;

  reg [`DATABUS_dim - 1:0] DataOut;

  wire [`DATABUS_dim - 1:0] DataIn;

  reg w_n, g_n, e_n, rp_n, wp_n, k, l_n;
  reg Info;
 
  reg [`Voltage_range]  Vdd;
  reg [`Voltage_range]  Vddq;
  reg [`Voltage_range]  Vpp;

  assign E_N  = e_n;
  assign RP_N = rp_n;
  assign W_N  = w_n;
  assign WP_N = wp_n;

  assign VDD  = Vdd;
  assign VDDQ = Vddq;
  assign VPP  = Vpp;
  
  assign L_N  = l_n;
  assign K  = k;
  
  assign Information = Info;

  mytask mtk(E_N, W_N, G_N, L_N, A, DQ, DataIn );


initial 

        begin
        Info =  1'b1;
        Vddq = 36'd1700;
        Vdd  = 36'd1700;
        Vpp  = 36'd2000 ;
        AddrBus   = `ADDRBUS_dim'hZZZZZZ;
        DataOut   = `DATABUS_dim'hzzzz;
        w_n  = 1'bZ;
        g_n  = 1'bZ;
        e_n  = 1'bZ;                     
        rp_n = 1'bZ;                     
        wp_n = 1'bZ;                     
        k    = 1'bZ;                     
        l_n  = 1'bZ;
        #(`Reset_time) rp_n = 1;
        end



initial
        begin
                #(`Reset_time) ;
                $display("Read");
                #10   mtk.read_m(`ADDRBUS_dim'h000000);
                $display("Read CFI");
                #100  mtk.read_CFI_Query(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000001);
                #100  mtk.read_m(`ADDRBUS_dim'h000002);
                #100  mtk.read_m(`ADDRBUS_dim'h000003);
                #100  mtk.read_m(`ADDRBUS_dim'h000004);
                #100  mtk.read_m(`ADDRBUS_dim'h000005);
                #100  mtk.read_m(`ADDRBUS_dim'h000010);
                #100  mtk.read_m(`ADDRBUS_dim'h000011);
                #100  mtk.read_m(`ADDRBUS_dim'h000012);
                #100  mtk.read_m(`ADDRBUS_dim'h000013);
                #100  mtk.read_m(`ADDRBUS_dim'h000014);
                #100  mtk.read_m(`ADDRBUS_dim'h000015);
                #100  mtk.read_m(`ADDRBUS_dim'h000016);
                #100  mtk.read_m(`ADDRBUS_dim'h000017);
                #100  mtk.read_m(`ADDRBUS_dim'h000018);
                #100  mtk.read_m(`ADDRBUS_dim'h000019);
                #100  mtk.read_m(`ADDRBUS_dim'h00001A);
                #100  mtk.read_m(`ADDRBUS_dim'h00001B);
                #100  mtk.read_m(`ADDRBUS_dim'h00001C);
                #100  mtk.read_m(`ADDRBUS_dim'h00001D);
                #100  mtk.read_m(`ADDRBUS_dim'h00001E);
                #100  mtk.read_m(`ADDRBUS_dim'h00001F);
                #100  mtk.read_m(`ADDRBUS_dim'h000020);
                #100  mtk.read_m(`ADDRBUS_dim'h000021);
                #100  mtk.read_m(`ADDRBUS_dim'h000022);
                #100  mtk.read_m(`ADDRBUS_dim'h000023);
                #100  mtk.read_m(`ADDRBUS_dim'h000024);
                #100  mtk.read_m(`ADDRBUS_dim'h000025);
                #100  mtk.read_m(`ADDRBUS_dim'h000026);
                #100  mtk.read_m(`ADDRBUS_dim'h000027);
                #100  mtk.read_m(`ADDRBUS_dim'h000028);
                #100  mtk.read_m(`ADDRBUS_dim'h000029);
                #100  mtk.read_m(`ADDRBUS_dim'h00002A);
                #100  mtk.read_m(`ADDRBUS_dim'h00002B);
                #100  mtk.read_m(`ADDRBUS_dim'h00002C);
                #100  mtk.read_m(`ADDRBUS_dim'h00002E);
                #100  mtk.read_m(`ADDRBUS_dim'h00002F);
                #100  mtk.read_m(`ADDRBUS_dim'h000030);
                #100  mtk.read_m(`ADDRBUS_dim'h000031);
                #100  mtk.read_m(`ADDRBUS_dim'h000032);
                #100  mtk.read_m(`ADDRBUS_dim'h000033);
                #100  mtk.read_m(`ADDRBUS_dim'h000034);
                #100  mtk.read_m(`ADDRBUS_dim'h000035);
                #100  mtk.read_m(`ADDRBUS_dim'h000036);
                #100  mtk.read_m(`ADDRBUS_dim'h000037);
                #100  mtk.read_m(`ADDRBUS_dim'h000038);
                #100  mtk.read_m(`ADDRBUS_dim'h000039);
                #100  mtk.read_m(`ADDRBUS_dim'h000040);
                #100  mtk.read_m(`ADDRBUS_dim'h000041);
                #100  mtk.read_m(`ADDRBUS_dim'h000042);
                #100  mtk.read_m(`ADDRBUS_dim'h000043);
                #100  mtk.read_m(`ADDRBUS_dim'h000044);
                #100  mtk.read_m(`ADDRBUS_dim'h000045);
                #100  mtk.read_m(`ADDRBUS_dim'h000046);
                #100  mtk.read_m(`ADDRBUS_dim'h000047);
                #100  mtk.read_m(`ADDRBUS_dim'h000048);
                #100  mtk.read_m(`ADDRBUS_dim'h000049);
                #100  mtk.read_m(`ADDRBUS_dim'h00004A);
                #100  mtk.read_m(`ADDRBUS_dim'h00004B);
                #100  mtk.read_m(`ADDRBUS_dim'h00004C);
                #100  mtk.read_m(`ADDRBUS_dim'h00004D);
                #100  mtk.read_m(`ADDRBUS_dim'h00004E);
                #100  mtk.read_m(`ADDRBUS_dim'h00004F);
                #100  mtk.read_m(`ADDRBUS_dim'h000050);
                #100  mtk.read_m(`ADDRBUS_dim'h000051);
                #100  mtk.read_m(`ADDRBUS_dim'h000052);
                #100  mtk.read_m(`ADDRBUS_dim'h778000);          
                #100  mtk.read_m(`ADDRBUS_dim'h778000);         
                #100  mtk.read_m(`ADDRBUS_dim'h7FFFFF);
                #100  mtk.read_array(`ADDRBUS_dim'h000000);
                #100  mtk.block_unLock(`ADDRBUS_dim'h000000);
                #100  mtk.program(`ADDRBUS_dim'h000000,  `DATABUS_dim'h0055 );
                #200  mtk.read_CFI_Query(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000001);
                #100  mtk.read_m(`ADDRBUS_dim'h000002);
                #100  mtk.read_m(`ADDRBUS_dim'h000003);
                #(`WordProgram_time) ;
                #100  mtk.read_array(`ADDRBUS_dim'h000000);
                #100  mtk.block_erase(`ADDRBUS_dim'h000000);
                #200  mtk.read_CFI_Query(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000001);
                #100  mtk.read_m(`ADDRBUS_dim'h000002);
                #100  mtk.read_m(`ADDRBUS_dim'h000003);
                #(`MainBlockErase_time  ) ;
                #100 mtk.read_array(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000001);
                #100  mtk.read_m(`ADDRBUS_dim'h000002);


                #200  mtk.read_m(`ADDRBUS_dim'h000003);
                #100  mtk.block_unLock(`ADDRBUS_dim'h000000);
                #100  mtk.read_CFI_Query(`ADDRBUS_dim'h000000);
                #100  mtk.read_m(`ADDRBUS_dim'h000001);
                #100  mtk.read_m(`ADDRBUS_dim'h000002);
                #100  mtk.read_m(`ADDRBUS_dim'h778000);          
                #100  mtk.read_array(`ADDRBUS_dim'h000000);
 
        end

        
endmodule
