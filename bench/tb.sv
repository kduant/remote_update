`timescale  1ns /1 ps

module tb ();
reg                     clk_p = 0;
wire                    clk_n; 

always
begin
    #(1s/200_000_000/2) clk_p = ~clk_p;
end
assign                  clk_n = ~clk_p;


reg                     rst_n = 1;
initial
begin
    #1us; rst_n = 0;
    #1us; rst_n = 1;
end

localparam              ADDR_W = 26;
localparam              DATA_W = 16;

logic                nor_wait;

logic                nor_adv_n;    // address valid
logic                nor_ce_n;     // chip enable
logic                nor_oe_n;
logic                nor_we_n;
logic                nor_wp_n;   // write protect

logic                nor_clk;
logic                nor_rst_n = 1;

logic [ADDR_W-1:00]  nor_addr;
tri [DATA_W-1:0] nor_data = 100'bz;
assign (weak1, weak0) nor_data = 0;

defparam  tb.TopEx01.remote_update_topEx01.nor_flash_commandEx01.FRAME_LEN = 128-1;
//defparam  tb.TopEx01.remote_update_topEx01.nor_flash_commandEx01.FRAME_LEN = 4;
defparam tb.TopEx01.control_testEx01.PROGRAM_256WORD_TIME = 100*750;
defparam tb.TopEx01.control_testEx01.ERASE_TIME_CNT = 1250;
//defparam tb.TopEx01.control_testEx01.ERASE_TIME_CNT = 1_000_000_000/8;
Top #
(
    .ADDR_W       (  26           ),
    .DATA_W       (  16           )
)
TopEx01
(
    .clk_p        (  clk_p        ),
    .clk_n        (  clk_n        ),
    .rst_n        (  rst_n        ),
    .nor_wait     (  nor_wait     ),
    //.nor_rst_n    (  nor_rst_n    ),
    .nor_adv_n    (  nor_adv_n    ),
    .nor_ce_n     (  nor_ce_n     ),
    .nor_oe_n     (  nor_oe_n     ),
    .nor_we_n     (  nor_we_n     ),
    .nor_wp_n     (  nor_wp_n     ),
    //.nor_clk      (  nor_clk      ),
    .nor_addr     (  nor_addr     ),
    .nor_data     (  nor_data     )
);


x28fxxxp30 DUT
(
    .A            (  nor_addr     ),
    .DQ           (  nor_data     ),
    .W_N          (  nor_we_n     ),
    .G_N          (  nor_oe_n     ),
    .E_N          (  nor_ce_n     ),
    .L_N          (  nor_adv_n    ),
    .K            (  nor_clk      ),
    .WAIT         (  nor_wait     ),
    .WP_N         (  nor_wp_n     ),
    .RP_N         (  nor_rst_n    ),
    .VDD          (  36'h6a4      ),
    .VDDQ         (  36'h6a4      ),
    .VPP          (  36'h7d0      ),
    .Info         (  1'b1         )
);
endmodule
