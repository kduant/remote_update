quit -sim
.main clear
set env(ISE_SIM)                F:/crack/ise14.7_questasim_10.6c_lib
vmap secureip                   $env(ISE_SIM)/secureip
vmap unisims_ver                $env(ISE_SIM)/unisims_ver
vmap unimacro_ver               $env(ISE_SIM)/unimacro_ver
vmap xilinxcorelib_ver          $env(ISE_SIM)/xilinxcorelib_ver

#工程所需要的文件
vlog -incr $env(ISE_SIM)/glbl.v;
vlog -incr -sv ../bench/tb.sv

vlog -incr +define+USE_ISE ../hdl/Top.v
vlog -incr +define+USE_ISE ../hdl/control_test.v

vlog -incr ../hdl/remote_update/src/remote_update_top.v
vlog -incr ../hdl/remote_update/src/write_convert.v
vlog -incr ../hdl/remote_update/ip/axi_fifo_16x1024/axi_fifo_16x1024.v

vlog -incr ../hdl/nor_flash_drive/src/nor_flash_command.v
vlog -incr ../hdl/nor_flash_drive/src/nor_flash_core.v

vlog -incr ../ip/sys_clk/clk_gen.v


vlog -incr +incdir+../bench/NU_28F00AP30_VG17 +incdir+../bench/NU_28F00AP30_VG17/sim   ../bench/NU_28F00AP30_VG17/code/28F00AP30.v



vsim -t ps -voptargs="+acc" +notimingchecks \
                                -L secureip \
                                -L unisims_ver \
                                -L unimacro_ver \
                                -L xilinxcorelib_ver \
                                glbl \
                                work.tb

log -r /*
radix 16

#view -title {wang} wave
#具体模块需要添加的信号
do wave.do

run 100us
