quit -sim
.main clear

#工程所需要的文件
#vlog -incr  ./top/top.v
#vlog -incr  ./stim/erase/block_erase.v

vlog -incr +incdir+..   ./top.v
#vlog -incr +incdir+..  ../stim/erase/block_erase.v
#vlog -incr +incdir+..  ../stim/erase/block_erase_test.v
#vlog -incr +incdir+..  ../stim/program/program_my.v
#vlog -incr +incdir+..  ../stim/read/single_syncr_read.v
vlog -incr +incdir+..  ../stim/program/buffered_program.v
#vlog -incr +incdir+..  ../stim/read/read.v
#vlog -incr +incdir+..  ../stim/read/page_read.v
vlog -incr +incdir+..    ../code/28F00AP30.v

vsim -t ps -novopt  work.top

log -r /*
radix 16

do wave.do

run 20us


