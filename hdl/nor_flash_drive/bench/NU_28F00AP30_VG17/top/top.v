//          _/             _/_/
//        _/_/           _/_/_/
//      _/_/_/_/         _/_/_/
//      _/_/_/_/_/       _/_/_/              ____________________________________________ 
//      _/_/_/_/_/       _/_/_/             /                                           / 
//      _/_/_/_/_/       _/_/_/            /                                 28F00AP30 / 
//      _/_/_/_/_/       _/_/_/           /                                           /  
//      _/_/_/_/_/_/     _/_/_/          /                                     1Gbit / 
//      _/_/_/_/_/_/     _/_/_/         /                                single die / 
//      _/_/_/ _/_/_/    _/_/_/        /                                           / 
//      _/_/_/  _/_/_/   _/_/_/       /                  Verilog Behavioral Model / 
//      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.7 / 
//      _/_/_/    _/_/_/ _/_/_/     /                                           /
//      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2010 Numonyx B.V. / 
//      _/_/_/      _/_/_/_/_/    /___________________________________________/ 
//      _/_/_/       _/_/_/_/      
//      _/_/          _/_/_/  
// 
//     
//             NUMONYX              
// Main Characteristics 
`include "include/def.h"
`include "include/data.h"




module top;

  // Signal Bus
  wire [`ADDRBUS_dim - 1:0] A;         // Address Bus 
  wire [`DATABUS_dim - 1:0] DQ;        // Data I/0 Bus
  // Control Signal
  wire WE_N;                            // Write Enable 
  wire OE_N;                            // Output Enable
  wire CE_N;                            // Chip Enable
  wire RST_N;                           // Reset
  wire WP_N;                           // Write Protect
  wire ADV_N;                            // Latch Enable
  wire CLK;                              // Clock
  wire WAIT;                           // Wait

  // Voltage signal rappresentad by integer Vector which correspond to millivolts
  wire [`Voltage_range] VCC;                     // Supply Voltage
  wire [`Voltage_range] VCCQ;                    // Supply Voltage for I/O Buffers
  wire [`Voltage_range] VPP;                     // Optional Supply Voltage for Fast Program & Erase  

  //wire STS;
  
  wire Info;                          // Activate/Deactivate info device operation 
  
  stimulus stim(A, DQ, WE_N, OE_N, CE_N, ADV_N, CLK, RST_N, WP_N, VCC, VCCQ, VPP, Info);
  x28fxxxp30 DUT(A, DQ, WE_N, OE_N, CE_N, ADV_N, CLK, WAIT, WP_N, RST_N, VCC, VCCQ, VPP, Info);

endmodule

