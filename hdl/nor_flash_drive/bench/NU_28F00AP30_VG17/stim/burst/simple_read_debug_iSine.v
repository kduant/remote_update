//          _/             _/_/
//        _/_/           _/_/_/
//      _/_/_/_/         _/_/_/
//      _/_/_/_/_/       _/_/_/              ____________________________________________ 
//      _/_/_/_/_/       _/_/_/             /                                           / 
//      _/_/_/_/_/       _/_/_/            /                                 28F00AP30 / 
//      _/_/_/_/_/       _/_/_/           /                                           /  
//      _/_/_/_/_/_/     _/_/_/          /                                     1Gbit / 
//      _/_/_/_/_/_/     _/_/_/         /                                single die / 
//      _/_/_/ _/_/_/    _/_/_/        /                                           / 
//      _/_/_/  _/_/_/   _/_/_/       /                  Verilog Behavioral Model / 
//      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.7 / 
//      _/_/_/    _/_/_/ _/_/_/     /                                           /
//      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2010 Numonyx B.V. / 
//      _/_/_/      _/_/_/_/_/    /___________________________________________/ 
//      _/_/_/       _/_/_/_/      
//      _/_/          _/_/_/  
// 
//     
//             NUMONYX              
// Include General define
`include "include/def.h"
// Include Characterization of the device
`include "include/data.h"
// Include CUI command define
`include "stim/AllTasks.h"


/***************************************************************************** 
*****************************************************************************/ 
`define DEBUG



module  stimulus(A, DQ, W_N, G_N, E_N, L_N, K, RP_N, WP_N, VDD, VDDQ, VPP, Information);
   
   // Signal Bus
  output [`ADDRBUS_dim - 1:0] A;         // Address Bus 
  output [`DATABUS_dim - 1:0] DQ;        // Data I/0 Bus
  // Control Signal
  output W_N;                            // Write Enable 
  output G_N;                            // Output Enable
  output E_N;                       // Chip Enable
  output RP_N;                           // Reset/Power-Down
  output WP_N;                           // Write protect

  // Voltage signal rappresentad by integer Vector which correspond to millivolts
  output [`Voltage_range] VDD;                      // Supply Voltage
  output [`Voltage_range] VDDQ;                     // Input/Output Supply Voltage
  output [`Voltage_range] VPP;                      // Optional supply voltage
  
  // Others Signal
  
  output K;                               // clock
  output L_N;                             // latch enable
  
  
 output Information;                    // Enable/Disable Information of the operation in the memory 
   
  reg [`ADDRBUS_dim - 1:0] AddrBus;

  reg [`DATABUS_dim - 1:0] DataOut;

  wire [`DATABUS_dim - 1:0] DataIn;

  reg w_n, g_n, e_n, rp_n, wp_n, k, l_n;
  reg Info;
  reg [36:0]  Vdd;
  reg [36:0]  Vddq;
  reg [36:0]  Vpp;
  
  assign E_N  = e_n;
  assign RP_N = rp_n;
  assign W_N  = w_n;
  assign WP_N = wp_n;

  assign VDD  = Vdd;
  assign VDDQ = Vddq;
  assign VPP  = Vpp;
  
  assign L_N  = l_n;

  assign Information = Info;


`ifdef DEBUG  
  mytask mtk(E_N, W_N, G_N, L_N, A, DQ, DataIn, Clock );
`else  
  mytask mtk(E_N, W_N, G_N, L_N, A, DQ, DataIn );
`endif
  `ifdef DEBUG
      assign K = Clock;
  `else
  assign K  = k;
  `endif
    wire k1;
    wire k2;
    wire k3;
    wire k4;
    wire k5;
    wire k6;
    wire k7;
    wire k8;

    reg clock_on = 0;

always   begin 
        #10 k  = !k;
    //    #10 k  = !k;
end

assign k1 = k2;
assign k2 = k3;
assign k3 = k4;
assign k4 = k5;
assign k5 = k6;
assign k6 = k7;
assign k7 = k8;
assign k8 = k;

initial 

        begin
        
        Info =  1'b1;
        Vddq = 36'd1700;
        Vdd  = 36'd1700;
        Vpp  = 36'd2000 ;
        AddrBus   = `ADDRBUS_dim'hZZZZZZ;
        DataOut   = `DATABUS_dim'hzzzz;
        w_n  = 1'bZ;
        g_n  = 1'bZ;
        e_n  = 1'bZ;                     
        rp_n = 1'bZ;                     
        wp_n = 1'bZ;                     
        k    = 1'b1;
        l_n  = 1'bZ;
        #(`Reset_time) rp_n = 1;
        end


initial
        begin
                #(`Reset_time) ;
                #10   mtk.read_m(`ADDRBUS_dim'h0000A2);
                #100  mtk.block_unLock(`ADDRBUS_dim'h0000A2);
                #100  mtk.program(`ADDRBUS_dim'h0000A2,  `DATABUS_dim'h0002 );
                 #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000A3,  `DATABUS_dim'h0003 );
                 #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000A1,  `DATABUS_dim'h0001 );
                 #(`WordProgram_time) ;
                 #100  mtk.program(`ADDRBUS_dim'h0000A0,  `DATABUS_dim'h0000 );
                 #(`WordProgram_time) ;
                   #100  mtk.program(`ADDRBUS_dim'h0000AD,  `DATABUS_dim'hC1AD );
               #(`WordProgram_time) ;
                   #100  mtk.program(`ADDRBUS_dim'h0000AE,  `DATABUS_dim'hC1AE );
               #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000AF,  `DATABUS_dim'hC1A0 );
               #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000B2,  `DATABUS_dim'h00B2 );
                 #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000B3,  `DATABUS_dim'h00B3 );
                 #(`WordProgram_time) ;
                #100  mtk.program(`ADDRBUS_dim'h0000B1,  `DATABUS_dim'h00B1 );
                 #(`WordProgram_time) ;
                 #100  mtk.program(`ADDRBUS_dim'h0000B0,  `DATABUS_dim'h00B0 );
                 #(`WordProgram_time) ;


                #1000 

$display("Configure CR");
//                                                          XLatency - WaitPol. - Reserved - Wait Before
//                 #1000                               15   14-11       10        9            8
   //             #100 mtk.set_configuration_register({1'b0, 4'b0101,    1'b0,      1'b0,       1'b0, 
//                                               Burst Type(NC) - Clock Edge -   NC -     Wrap Burst - Burst Length
//                                                      7         6          5-4          3           2-0
   //                                                  1'b0,      1'b1,        2'b00,     1'b0,       3'b111});    

    #100 mtk.set_configuration_register('h3dcf);
                
$display("burst_read");
                #1000
                clock_on = 1;
                $display("Read Array");
//                #1000  mtk.read_array(`ADDRBUS_dim'h0000A0);
               $display("burst_read");
//                @k;
                //#50 mtk.burst_debug(`ADDRBUS_dim'h0000AF);
                #50 mtk.burst_debug_with_clock(`ADDRBUS_dim'h0000AF);

#100000 $finish;
        end

        
endmodule
