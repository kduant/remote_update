/*
* 总线基本命令, Bus Operations
*/
`define BASIC_CMD_ASYNC_READ  16'h0001
`define BASIC_CMD_SYNC_READ   16'h0002
`define BASIC_CMD_WRITE       16'h0003
`define BASIC_CMD_DISABLE     16'h0004
`define BASIC_CMD_STANDBY     16'h0005
`define BASIC_CMD_RESET       16'h0006
`define BASIC_CMD_UNLOCK      16'h0007

/*
* flash基本命令
*/
`define FLASH_ERASE_BLOCK           8'h20 
`define FLASH_BUFFER_PROGRAM        8'hE8 
`define FLASH_READ_PAGE             8'hFF 
`define FLASH_RD_STATUS_REG         8'h70 
`define FLASH_CLR_STATUS_REG        8'h50 


//`define FLASH_CMD_ERASE         8'h



// localparam              READ_ARRAY = 8'hFF;
// localparam              READ_STATUS_REG = 8'h70;
// localparam              READ_DEV_ID = 8'h90;
// localparam              READ_CFI = 8'h98;
// localparam              CLEAR_STATUS_REG = 8'h50;
// 
// localparam              WORD_PROGRAM = 8'h40;
// localparam              BUF_PROGRAM = 8'hE8;
// localparam              BUF_PROGRAM_CONFIRM = 8'hD0;
// localparam              BEFP_SETUP = 8'h80;
// localparam              BEFP_CONFIRM = 8'hD0;
// 
// localparam              BLOCK_ERASE = 8'h20;
// localparam              BLOCK_ERASE_CONFIRM = 8'hD0;
// 
// localparam              SUSPEND = 8'hB0;
// localparam              RESUME = 8'hD0;
// 
// localparam              BLOCK_LOCK_SETUP = 8'h60;
// localparam              BLOCK_LOCK = 8'h01;
// localparam              BLOCK_UNLOCK = 8'hD0;
// localparam              OPT_REG = 8'hC0;
// 
// localparam              READ_CONF_REG_SETUP = 8'h60;
// localparam              READ_CONF_REG = 8'h03;
// 
// localparam              BLOCK_CHECK = 8'hBC;
// localparam              BLOCK_CHECK_CONFIRM = 8'hD0;

