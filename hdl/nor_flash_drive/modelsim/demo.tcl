quit -sim
.main clear
set env(ISE_SIM)                F:/crack/ise14.7_questasim_10.6c_lib
vmap secureip                   $env(ISE_SIM)/secureip
vmap unisims_ver                $env(ISE_SIM)/unisims_ver
vmap unimacro_ver               $env(ISE_SIM)/unimacro_ver
vmap xilinxcorelib_ver          $env(ISE_SIM)/xilinxcorelib_ver

#工程所需要的文件
vlog -incr $env(ISE_SIM)/glbl.v;

#工程所需要的文件
#vlog -incr  ./top/top.v
#vlog -incr  ./stim/erase/block_erase.v
vlog -incr +incdir+../src       ../src/nor_flash_command.v
vlog -incr +incdir+../src       ../src/nor_flash_core.v
vlog -incr +incdir+../bench/NU_28F00AP30_VG17 +incdir+../bench/NU_28F00AP30_VG17/sim   ../bench/NU_28F00AP30_VG17/code/28F00AP30.v
vlog -sv -incr  +incdir+../../common_interface_sim/axi_stream  +define+PRINT_AXI_STREAM_INFO  ../bench/nor_flash_tb.sv


vsim -t ps -voptargs="+acc" +notimingchecks \
                                -L secureip \
                                -L unisims_ver \
                                -L unimacro_ver \
                                -L xilinxcorelib_ver \
                                glbl \
                                work.nor_flash_tb

log -r /*
radix 16

do wave.do

run 20us

