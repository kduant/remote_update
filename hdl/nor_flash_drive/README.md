# nor_flash_drive
nor flash model: PC28F00AG18A
单位
# 添加仿真模型流程
1. 删除`code`, `include`, `stim`, `sim`, `top`以外的文件和文件夹

2. 修改`include`文件夹里文件的头文件包含路径
> 文件路径以`*.mpf`为准

3. 将`sim/memory1Gb.vmf`和`sim/CFImemory1Gb_symmetrical.vmf`文件copy到modelsim目录下


# 记录

## 2019.12.19
1. `20 d0`的擦除命令，在仿真时好像擦除的是整个flash
2. flash cell里的数据不是`ff`时，无法写入数据
3. 需要block unlock后，才可以写数据

## 2019.12.25

RST#    CLK     ADV#        CE#         OE#     WE#
                L_N         E_N         G_N     W_N
