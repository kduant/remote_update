`timescale  1 ns/1 ps
//`define DEBUG_CLK

module Top #
(
    parameter               ADDR_W = 26,
    parameter               DATA_W = 16
)
(
    input                       clk_p,
    input                       clk_n,
    input                       rst_n,
`ifdef DEBUG_CLK
`else
    input   wire                nor_wait,

    output  wire                nor_adv_n, // address valid
    output  wire                nor_ce_n,  // chip enable
    output  wire                nor_oe_n,
    output  wire                nor_we_n,
    //output  wire                nor_wp_n,  // 采集卡上不需要控制此端口

    //output  wire                nor_clk,    // 异步模式可以不使用时钟信号
    output  wire [ADDR_W-1:00]  nor_addr,
    inout   wire [DATA_W-1:00]  nor_data,
`endif
    //output  wire                test_port
    output       [15:00]        m_axis_tdata,
    output                      m_axis_tvalid,
    output                      m_axis_tlast
);


`ifdef USE_ISE
clk_gen clk_gen_i
(
    .RESET            (  !rst_n               ),
    .CLK_IN1_P        (  clk_p                ),
    .CLK_IN1_N        (  clk_n                ),
    .CLK_OUT1         (  clk_200m             ),  // 200M
    .CLK_OUT2         (  clk_125m             ),  // 125M
    .CLK_OUT3         (                       ),  // 10M
    .LOCKED           (  locked               )
);
`else
clk_gen clk_gen_i
(
    .reset            (  !rst_n               ),
    .clk_in1_p        (  clk_p                ),
    .clk_in1_n        (  clk_n                ),
    .CLK_OUT1         (  clk_200m             ),  // 200M
    .CLK_OUT2         (  clk_125m             ),  // 125M
    .CLK_OUT3         (                       ),  // 10M
    .LOCKED           (  locked               )
);
`endif

`ifdef DEBUG_CLK
    reg     [07:00]             cnt = 0;
    always @ (posedge clk_125m)
    begin
        cnt <= cnt + 1'b1;
    end
    assign                  test_port = cnt[7];
`else
wire                        rst;

assign               #1_000   rst = ~rst_n;


(* KEEP = "TRUE" *)wire [07:00]        flash_cmd;
(* KEEP = "TRUE" *)wire [ADDR_W-1:00]  flash_addr;     // 需要操作的flash地址

(* KEEP = "TRUE" *)wire                 write_axis_tvalid;
(* KEEP = "TRUE" *)wire                 write_axis_tlast;
(* KEEP = "TRUE" *)wire                 write_axis_tready;
(* KEEP = "TRUE" *)wire  [07:00]        write_axis_tdata;

//(* KEEP = "TRUE" *)wire                 m_axis_tvalid;
//(* KEEP = "TRUE" *)wire                 m_axis_tlast;
//(* KEEP = "TRUE" *)wire                 m_axis_tready;
//(* KEEP = "TRUE" *)wire  [DATA_W-1:00]  m_axis_tdata;

control_test #
(
    .ADDR_W           (  26                   ),
    .DATA_W           (  16                   )
)
control_testEx01
(
    .clk              (  clk_125m             ),
    .rst              (  rst                  ),
    .flash_cmd        (  flash_cmd            ),
    .flash_addr       (  flash_addr           ),
    .m_axis_tvalid    (  write_axis_tvalid    ),
    .m_axis_tlast     (  write_axis_tlast     ),
    .m_axis_tready    (  write_axis_tready    ),
    .m_axis_tdata     (  write_axis_tdata     )
);


remote_update_top #
(
    .ADDR_W           (  26                   ),
    .DATA_W           (  16                   )
)
remote_update_topEx01
(
    .clk              (  clk_125m             ),
    .rst              (  rst                  ),
    .flash_cmd        (  flash_cmd            ),
    .flash_addr       (  flash_addr           ),

    .s_axis_tvalid    (  write_axis_tvalid    ),
    .s_axis_tlast     (  write_axis_tlast     ),
    .s_axis_tready    (  write_axis_tready    ),
    .s_axis_tdata     (  write_axis_tdata     ),

    .m_axis_tvalid    (  m_axis_tvalid        ),
    .m_axis_tlast     (  m_axis_tlast         ),
    .m_axis_tready    (  1'b1        ),
    .m_axis_tdata     (  m_axis_tdata         ),
    .nor_wait         (  nor_wait             ),
    .nor_adv_n        (  nor_adv_n            ),
    .nor_ce_n         (  nor_ce_n             ),
    .nor_oe_n         (  nor_oe_n             ),
    .nor_we_n         (  nor_we_n             ),
    .nor_wp_n         (                       ),
    .nor_clk          (  nor_clk              ),
    .nor_addr         (  nor_addr             ),
    .nor_data         (  nor_data             )
);

//assign                  test_port = m_axis_tvalid & m_axis_tlast & (&m_axis_tdata);

assign                  test_adv_n = nor_adv_n;
assign                  test_ce_n = nor_ce_n;
assign                  test_oe_n = nor_oe_n;
assign                  test_we_n = nor_we_n;
//assign                  nor_clk = 1;

`endif
endmodule
