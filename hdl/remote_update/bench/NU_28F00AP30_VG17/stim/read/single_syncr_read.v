//          _/             _/_/
//        _/_/           _/_/_/
//      _/_/_/_/         _/_/_/
//      _/_/_/_/_/       _/_/_/              ____________________________________________ 
//      _/_/_/_/_/       _/_/_/             /                                           / 
//      _/_/_/_/_/       _/_/_/            /                                 28F00AP30 / 
//      _/_/_/_/_/       _/_/_/           /                                           /  
//      _/_/_/_/_/_/     _/_/_/          /                                     1Gbit / 
//      _/_/_/_/_/_/     _/_/_/         /                                single die / 
//      _/_/_/ _/_/_/    _/_/_/        /                                           / 
//      _/_/_/  _/_/_/   _/_/_/       /                  Verilog Behavioral Model / 
//      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.7 / 
//      _/_/_/    _/_/_/ _/_/_/     /                                           /
//      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2010 Numonyx B.V. / 
//      _/_/_/      _/_/_/_/_/    /___________________________________________/ 
//      _/_/_/       _/_/_/_/      
//      _/_/          _/_/_/  
// 
//     
//             NUMONYX              

// Include General define
`include "./include/def.h"
// Include Characterization of the device
`include "./include/data.h"
// Include CUI command define
`include "./stim/AllTasks.h"


/***************************************************************************** 
stim:    CFI query, read CFI bank 0, read status register
         read electronic signature, read block protection
         syncronous read 
  
*****************************************************************************/ 




module  stimulus(A, DQ, W_N, G_N, E_N, L_N, K, RP_N, WP_N, VDD, VDDQ, VPP, Information);
   
   // Signal Bus
  output [`ADDRBUS_dim - 1:0] A;         // Address Bus 
  output [`DATABUS_dim - 1:0] DQ;        // Data I/0 Bus
  // Control Signal
  output W_N;                            // Write Enable 
  output G_N;                            // Output Enable
  output E_N;                       // Chip Enable
  output RP_N;                           // Reset/Power-Down
  output WP_N;                           // Write protect

  // Voltage signal rappresentad by integer Vector which correspond to millivolts
  output [`Voltage_range] VDD;                      // Supply Voltage
  output [`Voltage_range] VDDQ;                     // Input/Output Supply Voltage
  output [`Voltage_range] VPP;                      // Optional supply voltage
  
  // Others Signal
  
  output K;                               // clock
  output L_N;                             // latch enable
  
  
 output Information;                    // Enable/Disable Information of the operation in the memory 
   
  reg [`ADDRBUS_dim - 1:0] AddrBus;

  reg [`DATABUS_dim - 1:0] DataOut;

  wire [`DATABUS_dim - 1:0] DataIn;

  reg w_n, g_n, e_n, rp_n, wp_n, k, l_n;
  reg Info;
 
  reg [`Voltage_range]  Vdd;
  reg [`Voltage_range]  Vddq;
  reg [`Voltage_range]  Vpp;

  assign E_N  = e_n;
  assign RP_N = rp_n;
  assign W_N  = w_n;
  assign WP_N = wp_n;

  assign VDD  = Vdd;
  assign VDDQ = Vddq;
  assign VPP  = Vpp;
  
  assign L_N  = l_n;
  assign K  = k;
  
  assign Information = Info;

  mytask mtk(E_N, W_N, G_N, L_N, A, DQ, DataIn );

always   begin 
        #10 k  = !k;
end


initial 

        begin
        
        Info =  1'b1;
        Vddq = 36'd1700;
        Vdd  = 36'd1700;
        Vpp  = 36'd2000 ;
        AddrBus   = `ADDRBUS_dim'hZZZZZZ;
        DataOut   = `DATABUS_dim'hzzzz;
        w_n  = 1'bZ;
        g_n  = 1'bZ;
        e_n  = 1'bZ;                     
        rp_n = 1'bZ;                     
        wp_n = 1'bZ;                     
        k    = 1'b1;
        l_n  = 1'bZ;
        #(`Reset_time) rp_n = 1;
        end


initial
        begin
                #(`Reset_time) ;
                #100  mtk.read_m(`ADDRBUS_dim'h000000);
                #200 mtk.set_configuration_register(`ADDRBUS_dim'h002841);
                $display("Read Signature");
                #1000  mtk.read_signature(`ADDRBUS_dim'h000000);
                #1000  mtk.burst(`ADDRBUS_dim'h000005);
                $display("Read CFI");
                #100   mtk.read_CFI_Query(`ADDRBUS_dim'h000000);
                $display("Burst");
                #1000   mtk.burst(`ADDRBUS_dim'h000010);
                #1000  mtk.burst(`ADDRBUS_dim'h000011);
                #1000  mtk.burst(`ADDRBUS_dim'h000012);
                #1000  mtk.burst(`ADDRBUS_dim'h000013);
                $display("Read Array");
                #1000  mtk.read_array(`ADDRBUS_dim'h000000);
                $display("Read Signature");
                #1000  mtk.read_signature(`ADDRBUS_dim'h000000);
                #1000  mtk.burst(`ADDRBUS_dim'h000000);           
                #1000  mtk.burst(`ADDRBUS_dim'h000002);           
                #1000  mtk.burst(`ADDRBUS_dim'h000081);          
                #1000  mtk.burst(`ADDRBUS_dim'h000085);           
                #1000  mtk.read_array(`ADDRBUS_dim'h000000);
                $display("Read Status Register");

                #1000 mtk.read_status_register(`ADDRBUS_dim'h000000);
                #1000  mtk.burst(`ADDRBUS_dim'h000000);
                #1000  mtk.burst(`ADDRBUS_dim'h000001);
                #1000  mtk.burst(`ADDRBUS_dim'h000002);
 
                #1000 
   #100000 $finish;

 end

        
endmodule
