//          _/             _/_/
//        _/_/           _/_/_/
//      _/_/_/_/         _/_/_/
//      _/_/_/_/_/       _/_/_/              ____________________________________________ 
//      _/_/_/_/_/       _/_/_/             /                                           / 
//      _/_/_/_/_/       _/_/_/            /                                 28F00AP30 / 
//      _/_/_/_/_/       _/_/_/           /                                           /  
//      _/_/_/_/_/_/     _/_/_/          /                                     1Gbit / 
//      _/_/_/_/_/_/     _/_/_/         /                                single die / 
//      _/_/_/ _/_/_/    _/_/_/        /                                           / 
//      _/_/_/  _/_/_/   _/_/_/       /                  Verilog Behavioral Model / 
//      _/_/_/   _/_/_/  _/_/_/      /                               Version 1.7 / 
//      _/_/_/    _/_/_/ _/_/_/     /                                           /
//      _/_/_/     _/_/_/_/_/_/    /           Copyright (c) 2010 Numonyx B.V. / 
//      _/_/_/      _/_/_/_/_/    /___________________________________________/ 
//      _/_/_/       _/_/_/_/      
//      _/_/          _/_/_/  
// 
//     
//             NUMONYX              

// ************************************ 
//
// User Data definition file :
//
//      here are defined all parameters
//      that the user can change
//
// ************************************ 
  
`define x00AP30E  // Select the device. Possible value are: 256P30B, 256P30T
                 //                                        512P30B, 512P30T, 512P30E  
                 //                                        00AP30B, 00AP30T, 00AP30E 
`ifdef  x256P30B 
`define organization "bottom"
`elsif  x256P30T
`define organization "top"        // top, bottom or symmetrical(only for 512Mbit and 1Gbit)
`elsif  x512P30B
`define organization "bottom"
`elsif  x512P30T
`define organization "top"
`elsif  x512P30E
`define organization "symmetrical"
`elsif  x00AP30B
`define organization "bottom"
`elsif  x00AP30T
`define organization "top"
`elsif  x00AP30E
`define organization "symmetrical"
`endif

`define BLOCKPROTECT "on"         // if on the blocks are locked at power-up
`define TimingChecks "on"         // on for checking timing constraints
`define t_access      100          // Access Time 100 ns, 110 ns 

`ifdef x256P30B
`define FILENAME_mem "memory.vmf" // Memory File Name
`elsif  x256P30T
`define FILENAME_mem "memory.vmf" 
`elsif  x512P30B
`define FILENAME_mem "memory512Mb.vmf"
`elsif  x512P30T
`define FILENAME_mem "memory512Mb.vmf"
`elsif  x512P30E
`define FILENAME_mem "memory512Mb.vmf"
`elsif  x00AP30B
`define FILENAME_mem "memory1Gb.vmf"
`elsif  x00AP30T
`define FILENAME_mem "memory1Gb.vmf"
`elsif  x00AP30E
`define FILENAME_mem "memory1Gb.vmf"
`endif



