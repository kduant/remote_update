remote_update

# 项目介绍
[原理](https://zhuanlan.zhihu.com/p/44232327)


# 原理
将网络传输过来的程序数据通过nor flash驱动写入到配置芯片里即可


# debug 记录

## 2019.12.20
写数据长度128Word

* 擦除地址设置为0，从0地址读写正常
* 擦除地址设置为0，从0x2000地址读写正常
* 擦除地址设置为0，从0x3000地址读写正常
* 擦除地址设置为0，从0x3f80地址读写正常
* 擦除地址设置为0，从0x3f82地址读写不正常
* 擦除地址设置为0，从0x3fc0地址读写不正常
* 擦除地址设置为0，从0x4000地址读写不正常

> block sizie = 0x4000 Word = 0x4000 * 2 Byte = 32768 Byte = 32KB
> 单次写的时候不能跨block
> 与仿真现象不一致

* 擦除地址设置为0x01，  从0x4000地址读写不正常
* 擦除地址设置为0x4000，从0x4000地址读写不正常
