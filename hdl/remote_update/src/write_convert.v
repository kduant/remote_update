/*=============================================================================
# FileName    :	recv_convert.v
# Author      :	author
# Email       :	email@email.com
# Description :	
# Version     :	1.0
# LastChange  :	2019-09-02 11:07:40
# ChangeLog   :	
=============================================================================*/
`timescale  1 ns/1 ps

module write_convert
(
    input                       clk,
    input                       rst,
    /*port*/

    input                       s_axis_tvalid,
    input                       s_axis_tlast,
    output                      s_axis_tready,
    input        [07:00]        s_axis_tdata,

    output                      m_axis_tvalid,
    output                      m_axis_tlast,
    input                       m_axis_tready,
    output       [15:00]        m_axis_tdata
);

reg                         write_s_axis_tvalid;
reg                         write_s_axis_tlast;
wire                        write_s_axis_tready;
reg   [15:00]               write_s_axis_tdata;


reg     [01:00]             byte_cnt;

always @ (posedge clk)
begin
    if(rst)
        byte_cnt <= 0;
    else if(s_axis_tvalid & s_axis_tready)
        byte_cnt <= byte_cnt + 1'b1;
    else
        byte_cnt <= 0;
end

always @ (posedge clk)
begin
    if(rst)
        write_s_axis_tvalid <= 0;
    else if(byte_cnt[0] == 1'b1)
        write_s_axis_tvalid <= 1;
    else
        write_s_axis_tvalid <= 0;
end

always @ (posedge clk)
begin
    if(rst)
        write_s_axis_tdata <= 0;
    else if(s_axis_tvalid & s_axis_tready)
        write_s_axis_tdata <= {write_s_axis_tdata[07:00], s_axis_tdata};
end

always @ (posedge clk)
begin
    write_s_axis_tlast <= s_axis_tlast;
end

assign                  s_axis_tready = write_s_axis_tready;



axi_fifo_16x1024  axi_fifo_16x1024_Ex01
(
    .s_aclk           (    clk                    ),
    .s_aresetn        (    ~rst                   ),
    .s_axis_tvalid    (    write_s_axis_tvalid    ),
    .s_axis_tready    (    write_s_axis_tready    ),
    .s_axis_tdata     (    write_s_axis_tdata     ),
    .s_axis_tlast     (    write_s_axis_tlast     ),
    .m_axis_tvalid    (    m_axis_tvalid          ),
    .m_axis_tready    (    m_axis_tready          ),
    .m_axis_tdata     (    m_axis_tdata           ),
    .m_axis_tlast     (    m_axis_tlast           )
);

endmodule

