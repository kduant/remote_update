/*=============================================================================
# FileName    :	remote_update_top.v
# Author      :	author
# Email       :	email@email.com
# Description :	
                数据输入接口为8bit宽度的axi-stream接口fifo,内部转换成16bit后使用

                数据输出接口为16bit宽度的axi-stream接口fifo, 不转换

                flash芯片的数据端口为16bit，所以要匹配下
# Version     :	1.0
# LastChange  :	2019-09-02 10:14:56
# ChangeLog   :	
=============================================================================*/
`timescale  1 ns/1 ps

module remote_update_top #
(
    parameter               ADDR_W = 26,
    parameter               DATA_W = 16
)
(
    input                       clk,
    input                       rst,

    input   wire [07:00]        flash_cmd,
    input   wire [ADDR_W-1:00]  flash_addr,     // 需要操作的flash地址

    input                       s_axis_tvalid,
    input                       s_axis_tlast,
    output                      s_axis_tready,
    input        [07:00]        s_axis_tdata,

    output  wire                m_axis_tvalid,
    output  wire                m_axis_tlast,
    input                       m_axis_tready,
    output  wire [DATA_W-1:00]  m_axis_tdata,

    /*
     * nor flash 引脚
     */
    input   wire                nor_wait,

    output  wire                nor_rst_n,
    output  wire                nor_adv_n,    // address valid
    output  wire                nor_ce_n,     // chip enable
    output  wire                nor_oe_n,
    output  wire                nor_we_n,
    output  wire                nor_wp_n,   // write protect

    output  wire                nor_clk,    // 异步模式可以不使用时钟信号
    output  wire [ADDR_W-1:00]  nor_addr,
    inout   wire [DATA_W-1:00]  nor_data
);


wire                        write_m_axis_tvalid;
wire                        write_m_axis_tlast;
wire                        write_m_axis_tready;
wire  [15:00]               write_m_axis_tdata;

wire                        read_s_axis_tvalid;
wire                        read_s_axis_tlast;
wire                        read_s_axis_tready;
wire  [15:00]               read_s_axis_tdata;

write_convert write_convertEx01
(
    .clk              (    clk                    ),
    .rst              (    rst                    ),
    .s_axis_tvalid    (    s_axis_tvalid          ),
    .s_axis_tlast     (    s_axis_tlast           ),
    .s_axis_tready    (    s_axis_tready          ),
    .s_axis_tdata     (    s_axis_tdata           ),
    .m_axis_tvalid    (    write_m_axis_tvalid    ),
    .m_axis_tlast     (    write_m_axis_tlast     ),
    .m_axis_tready    (    write_m_axis_tready    ),
    .m_axis_tdata     (    write_m_axis_tdata     )
);

nor_flash_command #
(
    .FRAME_LEN        (    128 - 1                ),
    .ADDR_W           (    26                     ),
    .DATA_W           (    16                     )
)
nor_flash_commandEx01
(
    .clk              (    clk                    ),
    .rst              (    rst                    ),
    .flash_cmd        (    flash_cmd              ),
    .flash_addr       (    flash_addr             ),
    .s_axis_tvalid    (    write_m_axis_tvalid    ),
    .s_axis_tlast     (    write_m_axis_tlast     ),
    .s_axis_tready    (    write_m_axis_tready    ),
    .s_axis_tdata     (    write_m_axis_tdata     ),
                                                             
    .m_axis_tvalid    (    m_axis_tvalid          ),
    .m_axis_tlast     (    m_axis_tlast           ),
    .m_axis_tready    (    m_axis_tready          ),
    .m_axis_tdata     (    m_axis_tdata           ),

    .nor_wait         (    nor_wait               ),
    .nor_rst_n        (    nor_rst_n              ),
    .nor_adv_n        (    nor_adv_n              ),
    .nor_ce_n         (    nor_ce_n               ),
    .nor_oe_n         (    nor_oe_n               ),
    .nor_we_n         (    nor_we_n               ),
    .nor_wp_n         (    nor_wp_n               ),
    .nor_clk          (    nor_clk                ),
    .nor_addr         (    nor_addr               ),
    .nor_data         (    nor_data               )
);
endmodule
