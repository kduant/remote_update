quit -sim
.main clear
set env(ise_sim)                f:/crack/ise14.7_questasim_10.6c_lib
vmap secureip                   $env(ISE_SIM)/secureip
vmap unisims_ver                $env(ISE_SIM)/unisims_ver
vmap unimacro_ver               $env(ISE_SIM)/unimacro_ver
vmap xilinxcorelib_ver          $env(ISE_SIM)/xilinxcorelib_ver

vlib work
vmap work work

#工程所需要的文件
vlog -incr $env(ISE_SIM)/glbl.v;
vlog -sv -incr +incdir+../../../fpga/common_interface_sim/axi_stream +incdir+../../../fpga/nor_flash_drive/src ../bench/remote_update_tb.sv


vlog -incr ../src/remote_update_top.v
vlog -incr ../ip/axi_fifo_16x1024/axi_fifo_16x1024.v
vlog -incr ../src/write_convert.v

vlog -incr +incdir+../../nor_flash_drive/src ../../nor_flash_drive/src/nor_flash_command.v
vlog -incr +incdir+../../nor_flash_drive/src ../../nor_flash_drive/src/nor_flash_core.v

vlog -incr +incdir+../bench/NU_28F00AP30_VG17 ../bench/NU_28F00AP30_VG17/code/28F00AP30.v

vsim -t ps -voptargs="+acc" +notimingchecks \
                                    -L secureip \
                                    -L unisims_ver \
                                    -L unimacro_ver \
                                    -L xilinxcorelib_ver \
                                    glbl \
                                    work.remote_update_tb

log -r /*
radix 16

#view -title {wang} wave
#具体模块需要添加的信号
do wave.do

run 50us
